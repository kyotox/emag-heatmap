<?php

namespace Tests\Feature;

use App\Models\PageVisit;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class JourneyTest extends TestCase
{
    use WithoutMiddleware;


    private $testData = array(
        "SGGG0001" => array(
            "https://www.google.ro",
            "https://www.emag.ro",
            "https://www.emag.ro/genius",
            "https://www.emag.ro/genius/romania",
        ),
        "SGXX0002" => array(
            "https://www.emag.ro",
            "https://www.emag.ro/genius",
            "https://www.emag.ro/genius/romania",
        )
    );

    /**
     * Add visits from 2 customers, match same journey
     *
     * @return void
     */
    public function testJourney()
    {
        foreach ($this->testData as $customer_id => $visits){
            foreach ($visits as $visit){
                $response = $this->postJson('api/add', ['url' => $visit,'type'=>'homepage','customer_id'=> $customer_id]);

                $response
                    ->assertStatus(200)
                    ->assertJson([
                        'success' => true,
                    ]);
                sleep(1); // otherwise all will be added in the same second
            }
        }

        $response = $this->postJson('api/get/sameJourney', ['journey' => $this->testData['SGXX0002']]);
        $content = json_decode($response->getContent(),true);

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
        $this->assertTrue(in_array('SGGG0001',$content['customers']));
        $this->assertTrue(in_array('SGXX0002',$content['customers']));

    }


    /**
     * Remove test cases
     *
     * @return void
     */
    public function tearDown(): void{
        PageVisit::whereIn('customer_id',array_keys($this->testData))->delete();
    }
}
