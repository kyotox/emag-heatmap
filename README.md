# Install

* Copy .env.example to .env
* Update database config. The user needs to have full rights on the database.
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1  #emagheatmap-db for docker container
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```
* Install dependencies
```
composer install
```
* Run migration
```
php artisan migrate
```
___

# Docker Install
```
docker-compose build app
docker-compose up -d
docker exec emagheatmap-app composer install
docker exec emagheatmap-app php artisan migrate
```
You can access app on http://localhost:8000
___
# API

First, a user can be created via Artisan command:
```
php artisan user:create Dragos dragos@emagia.ro pArO1a

** if using Docker make sure you execute this inside the container
```

# Generate token
POST /api/user/authenticate
```
{
    "email": "dragos@emagia.ro",
    "password": "pArO1a"
}
```
Response:
```
{
    "success": true,
    "token": "1|oa0Jw7GgEV4Y0ZQ8FwXpGtyHihmLkZZV03wk7hWV"
}
```
The response contains a Bearer token that needs to be added to all requests:
```
--header 'Authorization: Bearer 1|oa0Jw7GgEV4Y0ZQ8FwXpGtyHihmLkZZV03wk7hWV'
```

# Add customer visit
>POST /api/add
```
{
    "url": "http://www.gogu.ro",
    "type": "product", //available options product, category, static-page, checkout, homepage
    "customer_id": "S006443",
    "visit_time" : "2021-05-11T00:00:00+02:00" //optional, current server time will be used if no time supplied
}
```
Get customer visit by type or URL
>POST /api/get/visits
```
{
    "type": "homepage",
    "from" : "2021-05-10T01:02:30+04:00", //ISO8601 format
    "to" : "2021-12-11T01:02:30+04:00"
}
```
OR
```
{
    "url": "https://www.google.ro",
    "from" : "2021-05-10T01:02:30+04:00", //ISO8601 format
    "to" : "2021-12-11T01:02:30+04:00"
}
```

## Get customer journey
>POST /api/get/customerVisits
```
{
    "customer_id": "S006443"
}
```
Example response
```
{
    "success": true,
    "journey": [
        "https://www.emag.ro",
        "https://www.emag.ro/genius",
        "https://www.emag.ro/genius/oferte"
    ]
}
```
## Get customer with the same journey
Returns all customers with this specific journey
>POST /api/get/sameJourney
```
{
    "journey": [
        "https://www.emag.ro",
        "https://www.emag.ro/genius",
        "https://www.emag.ro/genius/romania"
    ]
}
```
Example response
```
{
    "success": true,
    "customers": [
        "S006443",
        "S006443X"
    ]
}
```
## Tests
There is one unit test, that adds several visits from 2 different customers, checking afterwards the journey
```
php artisan test

---OR---

docker exec emagheatmap-app php artisan test
```
