<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->group(function () {
    Route::post('add', '\App\Http\Controllers\VisitController@addVisit')->name('add');
    Route::post('get/visits', '\App\Http\Controllers\VisitController@getVisits');
    Route::post('get/customerVisits', '\App\Http\Controllers\VisitController@getCustomerJourney');
    Route::post('get/sameJourney', '\App\Http\Controllers\VisitController@findSameJourney');
});

Route::post('user/authenticate', '\App\Http\Controllers\UserController@authenticate');


Route::get( 'login', function () {
    // sanctum redirects automatically to login unless 'Accept' => 'application/json' header is sent
    // to avoid that error, this route has to be defined

    return response()->json([
        'success' => false,
        'error' => "Invalid token"
    ]);
}
)->name('login');
