<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class createUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {name} {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $user           = new \App\Models\User();
        $user->password = \Illuminate\Support\Facades\Hash::make($this->argument('password'));
        $user->email    = $this->argument('email');
        $user->name     = $this->argument('name');
        $user->save();

        echo "Created user: $user->name";
    }
}
