<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PageVisit extends Model
{

    use HasFactory;

    public $timestamps = false;
    const TYPES = [
        'product',
        'category',
        'static-page',
        'checkout',
        'homepage'];


    protected $fillable = [
        'customer_id',
        'url',
        'type'
    ];
}
