<?php

namespace App\Http\Controllers;

use App\Models\PageVisit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * API endpoint used for registering a visit on a specific URL
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request){

        if (!Auth::attempt(array('email' => $request->get('email'), 'password' => $request->get('password'))))
        {
            return response()->json([
                'success' => false,
                'error' => 'Invalid credentials'
            ]);
        }


        $token = Auth::user()->createToken('api-login');
        return response()->json([
            'success' => true,
            'token' => $token->plainTextToken
        ]);
    }

    /**
     * API endpoint used for getting visits count on a specified
     * timeframe, based on an URL or TYPE
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVisits(Request $request){
        $data = json_decode($request->getContent(), true);

        if(json_last_error() != JSON_ERROR_NONE){
            return response()->json([
                'success' => false,
                'errors' => ["Invalid JSON request"]
            ]);
        }

        $rules = [
            'url'   => 'required_without:type',
            'type'  => [
                'required_without:url',
                Rule::in(PageVisit::TYPES),
            ],
            'from' => ['required', 'date_format:Y-m-d\TH:i:sP'], //ISO8601
            'to'   => ['required', 'date_format:Y-m-d\TH:i:sP'], //ISO8601
        ];

        $validator = Validator::make($data, $rules);
        if (!$validator->passes()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()->all(),
            ]);
        }

        //make sure not both url and type are sent
        if(!empty($data['url']) && !empty($data['type'])){
            return response()->json([
                'success' => false,
                'errors' => ['You should send URL or TYPE, not both parameters']
            ]);
        }

        $from = Carbon::parse($data['from']);
        $to = Carbon::parse($data['to']);
        $visitsQ = PageVisit::whereBetween('visit_time', [$from, $to]);

        if(!empty($data['url'])){
            $visitsQ->where('url',$data['url']);
        } else { // we always have either url or type, validation takes care of this
            $visitsQ->where('type',$data['type']);
        }

        return response()->json([
            'success' => true,
            'count'   => $visitsQ->count()
        ]);
    }


    /**
     * API endpoint used for getting the journey of a specific customer, chronologically.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCustomerJourney(Request $request){
        $data = json_decode($request->getContent(), true);

        if(json_last_error() != JSON_ERROR_NONE){
            return response()->json([
                'success' => false,
                'errors' => ["Invalid JSON request"]
            ]);
        }

        $rules = [
            'customer_id'   => 'required'
        ];

        $validator = Validator::make($data, $rules);
        if (!$validator->passes()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()->all(),
            ]);
        }

        $visits = PageVisit::where('customer_id', $data['customer_id'])->orderBy('visit_time','ASC')->pluck('url')->toArray();

        return response()->json([
            'success' => true,
            'journey'   => $visits
        ]);
    }

    /**
     * API endpoint for finding customers with a specific journey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findSameJourney(Request $request){
        $data = json_decode($request->getContent(), true);

        if(json_last_error() != JSON_ERROR_NONE){
            return response()->json([
                'success' => false,
                'errors' => ["Invalid JSON request"]
            ]);
        }

        $rules = [
            'journey'   => ['required','array']
        ];

        $validator = Validator::make($data, $rules);
        if (!$validator->passes()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()->all(),
            ]);
        }

        $journey = implode('|',$data['journey']);
        $customers = \DB::table('page_visits')
            ->select('customer_id')
            ->groupBy('customer_id')
            ->havingRaw("GROUP_CONCAT(url ORDER BY visit_time SEPARATOR '|') LIKE ?", ["%$journey%"])
            ->pluck('customer_id')
            ->toArray();

        return response()->json([
            'success' => true,
            'customers'   => $customers
        ]);
    }
}
